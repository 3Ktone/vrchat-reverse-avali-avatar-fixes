There will be 3 categories of fixes in this fork:

    1. FBX - These will be model fixes that will be compatible with Blender.
    2. Textures - These will be fixes to the texture files. These will be compatible with GIMP.
    3. Unity - These will contain any Unity fixes implemented. It will also contain a UnityPackage to simplify importing.
